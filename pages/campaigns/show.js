import React, { Component } from 'react';
import Layout from '../../components/Layout';
import Crowdfunding from '../../ethereum/crowdfunding';
import web3 from '../../ethereum/web3';
import { Card, Grid, Button } from 'semantic-ui-react';
import DonateForm from '../../components/DonateForm';
import { Link } from '../../routes';

class CampaignShow extends Component {
    static async getInitialProps(props) {
        const crowdfunding = Crowdfunding(props.query.address);
        const info = await crowdfunding.methods.getInfo().call();

        return {
            address: props.query.address,
            minimumDonation: info[0],
            balance: info[1],
            requestsCount: info[2],
            donatorsCount: info[3],
            owner: info[4]
        };
    }

    renderCards() {
        const {
            balance, owner, minimumDonation, requestsCount, donatorsCount
        } = this.props;

        const items = [
            {
                header: owner,
                meta: 'Адрес создателя кампании',
                description: 'Создатель кампании. Имеет право создавать запросы на траты денежных средств.',
                style: {overflowWrap: 'break-word'}
            },
            {
                header: minimumDonation,
                meta: 'Минимальный размер пожертвования (wei)',
                description: 'Для участия в кампании вы должны внести сумму не менее этой.'
            },
            {
                header: requestsCount,
                meta: 'Запросы на трату ден. средств',
                description: 'Количество запросов на трату денежных средств, созданных автором кампании. Запросы утверждаются инвесторами.'
            },
            {
                header: donatorsCount,
                meta: 'Количество инвесторов',
                description: 'Общее количество адресов, хотя бы единожды пожертвовавших средства.'
            },
            {
                header: web3.utils.fromWei(balance, 'ether'),
                meta: 'Баланс кампании (eth)',
                description: 'Остаток средств на балансе контракта.'
            },
        ];

        return <Card.Group items={items} />;
    }

    render() {
        return (
            <Layout>
                <h1>Информация о кампании</h1>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={10}>
                            {this.renderCards()}
                        </Grid.Column>
                        <Grid.Column width={6}>
                            <DonateForm address={this.props.address} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Link route={`/campaigns/${this.props.address}/requests`}>
                                <a><Button primary>Список запросов</Button></a>
                            </Link>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Layout>
        );
    }
}

export default CampaignShow;