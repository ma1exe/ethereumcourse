import React, { Component } from 'react';
import { Form, Button, Message, Input } from 'semantic-ui-react';
import web3 from '../../../ethereum/web3';
import Crowdfunding from '../../../ethereum/crowdfunding';
import { Link, Router } from '../../../routes';
import Layout from '../../../components/Layout';

class RequestNew extends Component {
    state = {
        value: '',
        description: '',
        recipient: '',
        errorMessage: '',
        loading: false
    }

    static async getInitialProps(props) {
        const {address} = props.query;

        return {address};
    }

    onSubmit = async event => {
        event.preventDefault();

        const crowdfunding = Crowdfunding(this.props.address);
        const { description, value, recipient } = this.state;
        
        this.setState({loading: true, errorMessage: ''});

        try {
             const accounts = await web3.eth.getAccounts();
             await crowdfunding.methods
                .createRequest(description, web3.utils.toWei(value, 'ether'), recipient)
                .send({from: accounts[0]});
            Router.pushRoute(`/campaigns/${this.props.address}/requests`);
        } catch (error) {
            this.setState({errorMessage: error.message});
        }

        this.setState({loading: false});
    }

    render() {
        return (
            <Layout>
                <Link route={`/campaigns/${this.props.address}/requests`}>
                    <a>К списку запросов</a>
                </Link>
                <h1>Создание запроса</h1>
                <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
                    <Form.Field>
                        <label>Описание</label>
                        <Input 
                            value={this.state.description}
                            onChange={event =>
                                this.setState({description: event.target.value})}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Необходимая сумма</label>
                        <Input
                            label="eth"
                            labelPosition="right"
                            value={this.state.value}
                            onChange={event =>
                                this.setState({value: event.target.value})}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Получатель</label>
                        <Input 
                            value={this.state.recipient}
                            onChange={event =>
                                this.setState({recipient: event.target.value})}
                        />
                    </Form.Field>
                    <Message error header="Ой!" content={this.state.errorMessage} />
                    <Button primary loading={this.state.loading}>Создать</Button>
                </Form>
            </Layout>
        );
    }
}

export default RequestNew;