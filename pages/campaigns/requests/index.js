import React, { Component } from 'react';
import Layout from '../../../components/Layout';
import { Button, Table } from 'semantic-ui-react';
import { Link } from '../../../routes';
import Crowdfunding from '../../../ethereum/crowdfunding';
import RequestRow from '../../../components/RequestRow';

class RequestIndex extends Component {
    static async getInitialProps(props) {
        const {address} = props.query;
        const crowdfunding = Crowdfunding(props.query.address);
        const requestsCount = await crowdfunding.methods.getRequestsCount().call();
        const donatorsCount = await crowdfunding.methods.donatorsCount().call();

        const requests = await Promise.all(
            Array(parseInt(requestsCount))
                .fill()
                .map((element, index) => {
                    return crowdfunding.methods.requests(index).call();
                })
        );

        return {address, requests, donatorsCount };
    }

    renderRows() {
        return this.props.requests.map((request, index) => {
            return (
                <RequestRow
                    key={index}
                    id={index}
                    request={request}
                    address={this.props.address}
                    donatorsCount={this.props.donatorsCount}
                />
            );
        });
    }

    render() {
        const { Header, Row, HeaderCell, Body } = Table;

        return (
            <Layout>
                <h1>Список запросов</h1>
                <Link route={`/campaigns/${this.props.address}/requests/new`}>
                    <a>
                        <Button floated="right" style={{marginBottom: 10}} primary>Создать запрос</Button>
                    </a>
                </Link>
                <Table>
                    <Header>
                        <Row>
                            <HeaderCell>ID</HeaderCell>
                            <HeaderCell>Описание</HeaderCell>
                            <HeaderCell>Сумма</HeaderCell>
                            <HeaderCell>Получатель</HeaderCell>
                            <HeaderCell>Голосов за</HeaderCell>
                            <HeaderCell>Голосовать</HeaderCell>
                            <HeaderCell>Завершить</HeaderCell>
                        </Row>
                    </Header>
                    <Body>
                        {this.renderRows()}
                    </Body>
                </Table>
            </Layout>
        );
    }
}

export default RequestIndex;