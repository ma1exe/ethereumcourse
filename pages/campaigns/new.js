import React, { Component } from 'react';
import { Form, Button, Input, Message } from 'semantic-ui-react';
import Layout from '../../components/Layout';
import factory from '../../ethereum/factory';
import web3 from '../../ethereum/web3';
import { Router } from '../../routes';

class CampaignNew extends Component {

    state = {
		minimumDonation: '',
		errorMessage: '',
		loading: false
	};

    onSubmit = async (event) => {
		event.preventDefault();

		this.setState({loading: true, errorMessage: ''});

		try {
			const accounts = await web3.eth.getAccounts();
			await factory.methods.createCampaign(this.state.minimumDonation)
				.send({
					from: accounts[0]
				});

			Router.pushRoute('/');
		} catch (error) {
			this.setState({errorMessage: error.message});
		}

		this.setState({loading: false});
	}

    render() {
		return (
			<Layout>
				<h1>Новая кампания</h1>

				<Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
					<Form.Field>
						<label>Минимальная сумма для инвестирования</label>
						<Input 
							label="wei" 
							labelPosition="right" 
							value={this.state.minimumDonation}
							onChange={
								event => this.setState({minimumDonation: event.target.value})
							}
						/>
					</Form.Field>

					<Message error 
						header="Ой! Кажется, что-то пошло не так..."
						content={this.state.errorMessage}
					/>
					<Button loading={this.state.loading} primary>Создать</Button>
				</Form>
			</Layout>
		);
	}

}

export default CampaignNew;