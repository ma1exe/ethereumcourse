import React, { Component } from 'react';
import { Table, Button } from 'semantic-ui-react';
import web3 from '../ethereum/web3';
import Crowdfunding from '../ethereum/crowdfunding';
import { Router } from '../routes';

class RequestRow extends Component {
    state = {
        loading: false
    }

    onApprove = async () => {
        this.setState({loading: true});
        const crowdfunding = Crowdfunding(this.props.address);
        const accounts = await web3.eth.getAccounts();
        try {
            await crowdfunding.methods
                .approveRequest(this.props.id)
                .send({from: accounts[0]});
            Router.pushRoute(`/campaigns/${this.props.address}/requests`);
        } catch (error) {
            this.setState({loading: false});
            window.alert(error);
        }
    }

    onFinalize = async () => {
        this.setState({loading: true});
        const crowdfunding = Crowdfunding(this.props.address);
        const accounts = await web3.eth.getAccounts();
        try {
            await crowdfunding.methods
                .finalizeRequest(this.props.id)
                .send({from: accounts[0]});
            Router.pushRoute(`/campaigns/${this.props.address}/requests`);
        } catch (error) {
            this.setState({loading: false});
            window.alert(error);
        }
    }

    render() {
        const { Row, Cell } = Table;
        const { id, request, donatorsCount } = this.props;
        const readyToFinalize = request.votersCount > donatorsCount / 2;

        return (
            <Row disabled={request.complete} positive={readyToFinalize && !request.complete}>
                <Cell>{id}</Cell>
                <Cell>{request.description}</Cell>
                <Cell>{web3.utils.fromWei(request.value, 'ether')} ETH</Cell>
                <Cell>{this.props.address}</Cell>
                <Cell>{request.votersCount}/{donatorsCount}</Cell>
                <Cell>
                    {request.complete ? null : (
                        <Button 
                            loading={this.state.loading} 
                            color="green" 
                            basic 
                            onClick={this.onApprove}
                        >Поддержать</Button>
                    )}
                </Cell>
                <Cell>
                    {request.complete ? null : (
                        <Button 
                            loading={this.state.loading} 
                            color="purple" 
                            basic 
                            onClick={this.onFinalize}
                        >Завершить</Button>
                    )}
                </Cell>
            </Row>
        );
    }
}

export default RequestRow;