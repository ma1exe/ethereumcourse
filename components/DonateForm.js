import React, { Component } from 'react';
import { Form, Input, Message, Button } from 'semantic-ui-react';
import Crowdfunding from '../ethereum/crowdfunding';
import web3 from '../ethereum/web3';
import { Router } from '../routes';

class DonateForm extends Component {
    state = {
        value: '',
        errorMessage: '',
        loading: false
    };

    onSubmit = async event => {
        event.preventDefault();

        const crowdfunding = Crowdfunding(this.props.address);
        this.setState({loading: true, errorMessage: ''});

        try {
            const accounts = await web3.eth.getAccounts();
            await crowdfunding.methods.donate().send({
                from: accounts[0],
                value: web3.utils.toWei(this.state.value, 'ether')
            });

            Router.replaceRoute(`/campaigns/${this.props.address}`);
        } catch (error) {
            this.setState({errorMessage: error.message});
        }

        this.setState({loading: false, value: ''});
    };

    render() {
        return (
            <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
                <Form.Field>
                    <label>Принять участие</label>
                    <Input
                        label="eth" 
                        labelPosition="right" 
                        value={this.state.value}
                        onChange={event => this.setState({value: event.target.value})}
                    />
                </Form.Field>
                <Message error header="Ой! Кажется, что-то пошло не так..." content={this.state.errorMessage} />
                <Button loading={this.state.loading} primary>Инвестировать</Button>
            </Form>
        );
    }
}

export default DonateForm;