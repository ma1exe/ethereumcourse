pragma solidity ^0.4.23;

contract CrowdfundingFactory {
    address[] public deployedCampaigns;

    function createCampaign(uint minimum) public {
        address newCampaign = new Crowdfunding(minimum, msg.sender);
        deployedCampaigns.push(newCampaign);
    }

    function getDeployedCampaigns() public view returns(address[]) {
        return deployedCampaigns;
    }
}

contract Crowdfunding {
    struct Request {
        string description;
        uint value;
        address recipient;
        bool complete;
        
        mapping(address => bool) voters;
        uint votersCount;
    }
    
    Request[] public requests;
    address public owner;
    uint public minimumDonation;
    mapping(address => bool) public donators;
    uint public donatorsCount;
    
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    
    constructor(uint minimum, address creator) public {
        owner = creator;
        minimumDonation = minimum;
    }
    
    function donate() public payable {
        require(msg.value >= minimumDonation);
        if (donators[msg.sender] == false) {
            donators[msg.sender] = true;
            donatorsCount++;
        }
    }

    function createRequest(string description, uint value, address recipient) public onlyOwner {
        Request memory newRequest = Request({
            description: description,
            value: value,
            recipient: recipient,
            complete: false,
            votersCount: 0
        });

        requests.push(newRequest);
    }
    
    function approveRequest(uint index) public {
        Request storage request = requests[index];
    
        require(donators[msg.sender] && request.voters[msg.sender] == false);
        
        request.voters[msg.sender] = true;
        request.votersCount++;
    }
    
    function finalizeRequest(uint index) public onlyOwner {
        Request storage request = requests[index];
        
        require(request.votersCount > (donatorsCount / 2));
        require(!request.complete);
        
        request.recipient.transfer(request.value);
        request.complete = true;
    }

    function getInfo() public view returns(
        uint, uint, uint, uint, address
    ) {
        return (
            minimumDonation,
            this.balance,
            requests.length,
            donatorsCount,
            owner
        );
    }

    function getRequestsCount() public view returns(uint) {
        return requests.length;
    }
}