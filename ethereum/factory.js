import web3 from './web3';
import CampaignFactory from './compiled/CrowdfundingFactory.json';

const instance = new web3.eth.Contract(
	JSON.parse(CampaignFactory.interface), 
	'0x531CE34fd89a9fCFdeEc1DC193f4C1Bc2EBCDEBe'
);

export default instance;