import Web3 from 'web3';

let web3;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
	// мы находимся в браузере, и у пользователя есть Metamask
	web3 = new Web3(window.web3.currentProvider);
} else {
	// мы находимся на сервере, или у пользователя нет Metamask
	const provider = new Web3.providers.HttpProvider(
		'https://rinkeby.infura.io/1f2gJ6JDh29sPr1alFi6'
	);
	web3 = new Web3(provider);
}

export default web3;