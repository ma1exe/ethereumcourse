const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledFactory = require('./compiled/CrowdfundingFactory.json');
const fs = require('fs-extra');

const provider = new HDWalletProvider(
	'image sand large road chuckle achieve prison give tool flag glide brown', 
	'https://rinkeby.infura.io/1f2gJ6JDh29sPr1alFi6'
);

const web3 = new Web3(provider);

const deploy = async () => {
	const accounts = await web3.eth.getAccounts();
	console.log('Загружаю контракт с аккаунта', accounts[0]);
	const result = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
		.deploy({ data: compiledFactory.bytecode })
        .send({ from: accounts[0], gas: '1000000' });
    fs.outputFileSync(__dirname + '/address.txt', result.options.address);
	console.log('Адрес контракта', result.options.address);
};
deploy();
