const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledFactory = require('../ethereum/compiled/CrowdfundingFactory.json');
const compiledCrowdfunding = require('../ethereum/compiled/Crowdfunding.json');

let accounts;
let factory;
let crowdfundingAddress;
let crowdfunding;

beforeEach(async () => {
    accounts = await web3.eth.getAccounts();

    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
        .deploy({data: compiledFactory.bytecode})
        .send({from: accounts[0], gas: '1000000'});

    await factory.methods.createCampaign('100')
        .send({from: accounts[0], gas: '1000000'});

    [crowdfundingAddress] = await factory.methods.getDeployedCampaigns().call();
    crowdfunding = await new web3.eth.Contract(
        JSON.parse(compiledCrowdfunding.interface),
        crowdfundingAddress
    ); // экземпляр контракта
});

describe('Кампания', () => {
    it('Контракт фабрики загружен в сеть, кампания создана', () => {
        assert.ok(factory.options.address);
        assert.ok(crowdfunding.options.address);
    });
    it('Создатель кампании записан в переменную owner', async () => {
        const owner = await crowdfunding.methods.owner().call();
        assert.ok(owner, accounts[0]);
    });
    it('Donate: требование мин. суммы соблюдено, участник записан в список инвесторов', async () => {
        try {
            await crowdfunding.methods.donate().send({value: '1000', from: accounts[1]});
        } catch (error) {
            assert.fail('Отправленная сумма меньше необходимой!');
        }
        const isDonator = await crowdfunding.methods.donators(accounts[1]).call();
        assert.ok(isDonator);
    });
    it('Запрос создан корректно', async () => {
        try {
            await crowdfunding.methods.createRequest('Тестовый запрос', 1000, accounts[3])
                .send({from: accounts[0], gas: '1000000'});
        } catch (error) {
            assert.fail('Аккаунт не является создателем кампании!');
        }
        const description = await crowdfunding.methods.requests(0).call();
        assert.ok(description, 'Тестовый запрос');
    });
    it('Финальный тест', async () => {
        await crowdfunding.methods.donate().send({
            from: accounts[1],
            value: web3.utils.toWei('5', 'ether')
        });
        await crowdfunding.methods
            .createRequest('Сбор средств на мопед', web3.utils.toWei('5', 'ether'), accounts[5])
            .send({from: accounts[0], gas: '1000000'});
        await crowdfunding.methods.approveRequest(0).send({
            from: accounts[1],
            gas: '1000000'
        });
        await crowdfunding.methods.finalizeRequest(0).send({
            from: accounts[0],
            gas: '1000000'
        });
        let balance = await web3.eth.getBalance(accounts[5]);
        balance = web3.utils.fromWei(balance, 'ether');
        balance = parseFloat(balance);
        assert.ok(balance > 104);
    });
});